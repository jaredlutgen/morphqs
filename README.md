# Morpheus Quickshots - README

## What is this?

This is a collection of Morpheus use cases that can be deployed for POCs and demos. Each use case is a self-contained that can be deployed in a few minutes.